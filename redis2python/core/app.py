import asyncio
import signal
import uvloop
import setproctitle
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
from redis2python.core.processors.base import BaseProcessor
from redis2python.core.processors.mixins import BrpopMixin



class Redis2Python(BrpopMixin):
    """
    TODO: explain what it does.
    """
    def __init__(
            self,
            name='redis2python',
            processor=BaseProcessor,
            queues=None,
            use_uvloop=False,
            debug=False,
            redis_host='redis://localhost',
            redis_port=6380):

        assert queues is not None, 'queues is missing.'
        # set instance values
        self.NAME = name
        self.DEBUG = debug
        self.REDIS2PYTHON_QUEUES = queues
        self.REDIS2PYTHON_UVLOOP = use_uvloop
        self.REDIS2PYTHON_DEBUG_LOG = debug
        self.REDIS2PYTHON_REDIS_HOST = redis_host
        self.REDIS2PYTHON_REDIS_PORT = redis_port
        self.REDIS2PYTHON_REDIS_URL = self._create_redis_url()
        self.REDIS2PYTHON_PROCESSOR = processor
        self.EXECUTORS = []
        # set the process title
        self._set_proctitle(self.NAME)
        # set the loop
        self._set_loop()
        # create the workers
        self.create_workers_messages = self._create_workers()
        # register signals for properly leave tasks and loop
        self._register_signals()
        # apply loop execption handler
        self.loop.set_exception_handler(self._handle_async_exception)

    def run(self):
        # run the process
        # informatives logs
        self.display_informations()
        # start the loop
        self.loop.run_forever()
        print('\n{} stopped.'.format(self.NAME))

    def _create_redis_url(self):
        url = "{}:{}".format(self.REDIS2PYTHON_REDIS_HOST, self.REDIS2PYTHON_REDIS_PORT)
        return url
    def _set_proctitle(self, title='redis2python'):
        # set the pocess title
        setproctitle.setproctitle(title)

    def _set_loop(self):
        # choose loop type
        if self.REDIS2PYTHON_UVLOOP:
            self.loop_name = "uvloop"
            self.loop = uvloop.new_event_loop()
            asyncio.set_event_loop(self.loop)
        else:
            self.loop_name = "asyncio"
            self.loop = asyncio.new_event_loop()
            asyncio.set_event_loop(self.loop)

    def _register_signals(self):
        # register signals when leaving the process
        self.loop.add_signal_handler(signal.SIGQUIT, self._shutdown, signal.SIGQUIT)
        self.loop.add_signal_handler(signal.SIGTERM, self._shutdown, signal.SIGTERM)
        self.loop.add_signal_handler(signal.SIGINT, self._shutdown, signal.SIGINT)
        self.loop.add_signal_handler(signal.SIGWINCH, self._shutdown, signal.SIGWINCH)
        self.loop.add_signal_handler(signal.SIGUSR1, self._shutdown, signal.SIGUSR1)
        self.loop.add_signal_handler(signal.SIGABRT, self._shutdown, signal.SIGABRT)

    def display_informations(self):
        # display the process settings
        print(self.create_workers_messages)
        print("--- {} settings:".format(self.NAME))
        print("      - debug: {}".format(self.REDIS2PYTHON_DEBUG_LOG))
        print("      - loop: {}".format(self.loop_name))
        print('      - executor number: {}'.format(len(self.EXECUTORS)))
        print("      - redis port {}".format(self.REDIS2PYTHON_REDIS_PORT))

    def _shutdown(self, sender):
        # TODO: https://bugs.python.org/issue23548
        loop = asyncio.get_event_loop()
        if sender.name in ["SIGWINCH", "SIGTERM", ]:
            return None
        for executor in self.EXECUTORS:
            executor.shutdown(wait=True)
        for task in asyncio.Task.all_tasks():
            task.cancel()
        # stop the loop
        loop.stop()
        # close the loop
        loop.close()

    def _get_executor(self, pool_type=None, max_workers=None):
        # choose pool executor
        if pool_type == "process":
            self.executor_name = "ProcessPoolExecutor"
            executor = ProcessPoolExecutor(max_workers=max_workers)
        elif pool_type == "thread":
            self.exceutor_name = "ThreadPoolExecutor"
            executor = ThreadPoolExecutor(max_workers=max_workers)
        else:
            raise Exception("REDIS2PYTHON_WORKER_TYPE should be process or thread.")
        return executor

    def _handle_async_exception(self, loop, context):
        # Overide this method to handle your own exception
        future = context.get('future')
        try:
            print(future.exception())
        except:
            pass
        return loop, context
