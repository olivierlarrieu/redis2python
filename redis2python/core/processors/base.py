

class BaseProcessor():
    @classmethod
    async def process(self, content_bytes, queue_name, loop, executor, aioredis_client_lpush):
        raise NotImplementedError
