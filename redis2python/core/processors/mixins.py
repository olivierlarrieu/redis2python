import asyncio
import aioredis


class BrpopMixin():
    
    def _create_workers(self):
        # create redis workers for brpop specif needs
        create_workers_messages = "declared queues:\n"
        tasks = []
        if not self.REDIS2PYTHON_QUEUES.items():
            raise Exception("queue bad configuration")
        for name, value in self.REDIS2PYTHON_QUEUES.items():
            # value is a list
            for conf in value:
                executor = self._get_executor(pool_type=conf.get('type'), max_workers=conf.get('max_workers'))
                self.EXECUTORS.append(executor)
                level = conf.get('rlevel')
                newname = '{}:rlevel_{}:prio_5'.format(name, level)
                tasks.append(
                    asyncio.ensure_future(self.brpop(newname, self.loop, executor, level))
                )
                create_workers_messages += "      - num_workers={} type={} name={}\n".format(conf.get('max_workers'), conf.get('type'), newname)
        return create_workers_messages

    async def brpop(self, queue_name, loop, executor, level):
        '''
        An asyncio worker which listen a redis queue and run
        in executor self.REDIS2PYTHON_PROCESSOR which is a BaseProcessor object
        '''
        aioredis_client_brpop = await aioredis.create_redis(
            'redis://localhost:{}'.format(self.REDIS2PYTHON_REDIS_PORT),
            loop=loop
        )
        aioredis_client_lpush = await aioredis.create_redis(
            'redis://localhost:{}'.format(self.REDIS2PYTHON_REDIS_PORT),
            loop=loop
        )
        while 1:
            key, content_bytes = await aioredis_client_brpop.brpop(queue_name)
            asyncio.ensure_future(
                self.REDIS2PYTHON_PROCESSOR.process(
                    content_bytes,
                    key,
                    loop,
                    executor,
                    aioredis_client_lpush
                )
            )