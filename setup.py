from distutils.core import setup

setup(name='redis2python',
      version='1.0.0',
      description='asyncio redis worker which execute python code',
      url='https://olivierlarrieu@bitbucket.org/olivierlarrieu/redis2python.git',
      author='Larrieu Olivier',
      author_email='larrieuolivierad@gmail.com',
      license='MIT',
      install_requires=['aioredis', 'uvloop', ],
      packages=['redis2python'],
      package_data = {'redis2python' : ['core/*', 'core/processors/*', ] },
)